/* 
 * File:   Yilan.hpp
 * Author: kyellow
 *
 * Created on December 11, 2012, 3:13 PM
 */

#ifndef YILAN_HPP
#define	YILAN_HPP

#include "kup.hpp"
#include <GL/glut.h>                // OPENGL glut ktüphanesini kullan
#include <vector>

//Hızı ayarlamak için gerekli  periyot sabitleri
#define PERIYOTMIN 10           //Minimum periyot
#define PERIYOTMAX 1000         //Maksimum periyot
#define PERIYOTSTEP 0.1         //Periyot adımı=0.1

//Yüzeyin Listesi için sabitler
#define YUZEY_COLOR 0.6          //yüzeyin rengi(Mavi)
#define YUZEY_ALPHA 0.3          //yüzey rengini şeffaflığı
#define YUZEY_LINE_COLOR 1.0     //yüzey çizgilerinin rengi
#define YUZEY_LINE_ALPHA 0.3     //yüzey çizgilerinin şeffaflığı

/*
 * \brief Yılan sınıfı, oyun alanındaki yılanı temsil eder
 */
class Yilan {

    enum Yon {Sag,Sol,Yukari,Asagi};      //sağ,sol,yukarı,aşağı

public:
    	/*
	 * \brief Varsayılan yapılandırıcı
	 */
	Yilan();   
	/*
	 * \brief Verilen küp yılan üzerinde mi kontol eder.
	 * \param in_orig Küp nesnesini alır
	 */
	bool operator==(const Kup& in_orig);
	/*!
	 * \brief Sanal yıkıcı
	 */
	virtual ~Yilan();
	/* 
	* OpenGL ön hazırlıkları için fonksiyonu
	*/ 
	void initOpenGL();
	/* 
	 * \brief Yılanın periyodik hareketlerinin kontrolünü yapan fonksiyonu 
	 */
	void periyodikKontrol();
	/* 
	 * \brief Yılanın klavye hareketlerinin kontrolünü yapan fonksiyonu 
	 */
	void hareketKontrol(int tus);
  	/* 
	 * \brief Oyunu sahneleyen fonksiyon 
	 */		
	void sahnele();

public:
	// Oyun alanı değerleri
	int genislik;		///< oyun alanı genişliği
	int yukseklik;		///< oyun alanı yüksekliği
	int derinlik;		///< oyun alanı derinliği
	int periyot;		///< oyununu periyot değeri

private :
  	/* 
	 * \brief Yılanı yeniden başlatma fonksiyonu 
	 */		
	void yenidenBasla();   
	/* 
	 * \brief Yılanın hareket fonksiyonu 
	 */
	void tasi(Yon in_yonTusu); 
	/* 
	 * \brief Yılanı çizen fonksiyon
	 */
	void ciz();
	/* 
	 * \brief Puan durumunu cizen fonsiyon
	 */
	void puaniCiz();  
	/* 
	 * \brief Yılanın yemini cizen fonsiyon
	 */
	void yemiCiz();
	/* 
	 * \brief Yemin yerini ayarlayan fonksiyon
	 */
	void yemYeriniAyarla(); 

	Kup yem_;    			///< yem
	std::vector<Kup> govde_; 	///< yılanın gövdesi
	Yon yon_;     			///< yön bilgisi
	int baslaCan_;    		///< başlangıç canı

	// Oynama değerleri
	bool duraklama_;
	int  skor_;
	int  kalanCan_;
	bool yuzey_;

	// OpenGL Listeleri
	GLuint yemList;
	GLuint kupList;
	GLuint yuzeyList;
    
};

#endif	/* YILAN_HPP */

