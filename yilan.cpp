/* 
 * File:   Yilan.cpp
 * Author: kyellow
 * 
 * Created on December 11, 2012, 3:13 PM
 */

#include "yilan.hpp"

#include <algorithm>
#include <sstream>      // std::stringstream
#include <iomanip>      // std::setprecision

using namespace std;

/* 
 *Yılan sınıfına ait yapılandırıcı 
 */
Yilan::Yilan() 
	:yem_(),periyot(200),yuzey_(true),
	genislik(50),yukseklik(1),derinlik(50),
	baslaCan_(3),kalanCan_(3),skor_(0)	 //Başlangıc yasam puanı=3   
{
    srand(1);

    this->yenidenBasla();
}

 /* 
  * OpenGL ön hazırlıkları için fonksiyonu
  */ 
void Yilan::initOpenGL()
{
     glClearColor (0.0, 0.0, 0.0, 0.0);  //Arka plan siyah
    
     glShadeModel (GL_SMOOTH);
     //glShadeModel (GL_FLAT);
     glEnable(GL_COLOR_MATERIAL);

     //glEnable(GL_DEPTH_TEST);
     //glDepthMask(GL_FALSE);

     //GLfloat light_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
     GLfloat light_ambient[] = { 1.0, 1.0, 1.0, 1.0 };
     GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
     GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
     //GLfloat light_position[] = { 10, 10, 20, 0.0 };

     //Işık kaynağının özelliklerinin tanımlanması
     glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
     glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
     glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
     //glLightfv(GL_LIGHT0, GL_POSITION, light_position);

     //Işık ve ışıklandırmayı aktif et
     glEnable(GL_LIGHT0);
     glEnable(GL_LIGHTING);

     //renk karışımını aç
     glEnable (GL_BLEND);
     glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
    
/////////////////////////////////////Listeleri tanımı:

     //yem için liste
     yemList = glGenLists(1);
     glNewList(yemList, GL_COMPILE);
    	  glutSolidSphere (0.5, 200, 200);      //Yem küre şeklinde    
     glEndList();

     /*Saydam bir kup oluşturulması*/

     const GLfloat vertices0[] = {
          0.5, -0.5, 0.5,  -0.5, -0.5, 0.5,  -0.4, -0.4, 0.5,   0.4, -0.4, 0.5,
          -0.5,0.5, 0.5, 0.5, 0.5, 0.5,  0.4, 0.4, 0.5,  -0.4, 0.4, 0.5
          };


     const GLfloat colors0[] = {
          KUP_RGB, KUP_AH, KUP_RGB, KUP_AH, KUP_RGB, KUP_AL, KUP_RGB, KUP_AL,
          KUP_RGB, KUP_AH, KUP_RGB, KUP_AH, KUP_RGB, KUP_AL, KUP_RGB, KUP_AL
     };

   //  #define KUP_NORMAL 0.0f, 0.0f, 1.0f
  //   const GLfloat normals0[] = {
   //       KUP_NORMAL, KUP_NORMAL, KUP_NORMAL, KUP_NORMAL, KUP_NORMAL, KUP_NORMAL, KUP_NORMAL, KUP_NORMAL
   //  };

     const GLubyte indices0[] = {
          0, 1, 2, 3,
          1, 4, 7, 2,
          4, 5, 6, 7,
          5, 0, 3, 6,
          3, 2, 7, 6
     };

     glEnableClientState(GL_VERTEX_ARRAY);
     glEnableClientState(GL_COLOR_ARRAY);
     //glEnableClientState(GL_NORMAL_ARRAY);
     glColorPointer(4, GL_FLOAT, 0, colors0);
     //glNormalPointer(GL_FLOAT, 0, normals0);
     glVertexPointer(3, GL_FLOAT, 0, vertices0);

     //Küp için liste
     kupList = glGenLists(1);
     glNewList(kupList, GL_COMPILE);
          glPushMatrix();
               //#define KUP_RGB2 1.0f, 0.0f, 0.0f
               //glColor4f(KUP_RGB2, 0.5);
               //glutSolidKUP (1);
               glDrawElements(GL_QUADS, 20, GL_UNSIGNED_BYTE, indices0);
               glRotatef(90, 0.0, 1.0, 0.0);
               glDrawElements(GL_QUADS, 20, GL_UNSIGNED_BYTE, indices0);
               glRotatef(90, 0.0, 1.0, 0.0);
               glDrawElements(GL_QUADS, 20, GL_UNSIGNED_BYTE, indices0);
               glRotatef(90, 0.0, 1.0, 0.0);
               glDrawElements(GL_QUADS, 20, GL_UNSIGNED_BYTE, indices0);
               glRotatef(90, 1.0, 0.0, 0.0);
               glDrawElements(GL_QUADS, 20, GL_UNSIGNED_BYTE, indices0);
               glRotatef(180, 1.0, 0.0, 0.0);
               glDrawElements(GL_QUADS, 20, GL_UNSIGNED_BYTE, indices0);
          glPopMatrix();
     glEndList();

     const float worldX = this->genislik, 
                 worldY = this->yukseklik, 
                 worldZ = this->derinlik;

     const GLfloat vertices1 [] = {
          -0.5f, -0.5f, -0.5f,  -0.5f, worldY-0.5f, -0.5f,
          worldX-0.5f, worldY-0.5f, -0.5f,  worldX-0.5f, -0.5f, -0.5f,
          -0.5f, -0.5f, worldZ-0.5f,  -0.5f, worldY-0.5f, worldZ-0.5f,
          worldX-0.5f, worldY-0.5f, worldZ-0.5f,  worldX-0.5f, -0.5f, worldZ-0.5f
     };

     const GLubyte indices1 [] = {
          0, 1, 2, 3,
          7, 6, 5, 4,
          1, 0, 4, 5,
          3, 2, 6, 7,
          2, 1, 5, 6,
          0, 3, 7, 4
     };

     glDisableClientState(GL_COLOR_ARRAY);
     glVertexPointer(3, GL_FLOAT, 0, vertices1);
     
     // Yüzey için liste
     yuzeyList = glGenLists(1);
     glNewList(yuzeyList, GL_COMPILE);
          glColor4f(YUZEY_COLOR, 0.0, 0.0, YUZEY_ALPHA);
          glDrawElements(GL_QUADS, 8, GL_UNSIGNED_BYTE, indices1);
          glColor4f(YUZEY_LINE_COLOR, 0.0, 0.0, YUZEY_LINE_ALPHA);
          glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, indices1);
          glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, indices1+4);

          glColor4f(0.0, YUZEY_COLOR, 0.0, YUZEY_ALPHA);
          glDrawElements(GL_QUADS, 8, GL_UNSIGNED_BYTE, indices1+8);
          glColor4f(0.0, YUZEY_LINE_COLOR, 0.0, YUZEY_LINE_ALPHA);
          glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, indices1+8);
          glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, indices1+12);

          glColor4f(0.0, 0.0, YUZEY_COLOR, YUZEY_ALPHA);
          glDrawElements(GL_QUADS, 8, GL_UNSIGNED_BYTE, indices1+16);
          glColor4f(0.0, 0.0, YUZEY_LINE_COLOR, YUZEY_LINE_ALPHA);
          glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, indices1+16);
          glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, indices1+20);
     glEndList();
}


/* 
 *Yıkıcı fonksiyonu 
 */
Yilan::~Yilan()
{
}

/* 
 * Yılanın periyodik hareketlerinin kontrol fonksiyonu 
 */
void Yilan::periyodikKontrol()
{
	if(duraklama_) return;      //oyun durmuş ise çık

	Kup yeniBas     = govde_.front();
	Kup yeniKuyruk  = govde_.back();

	switch(yon_){
		case Sag:       //Sağ ok tuşuna basıldı ise  
			yeniBas.x = (yeniBas.x + 1) % genislik;
		break;
		case Sol:       //Sol ok tuşuna basıldı ise  
			yeniBas.x = (yeniBas.x == 0) ? genislik - 1 : yeniBas.x - 1;
		break;	
		case Yukari:     //Yukarı ok tuşuna basıldı ise    
			yeniBas.z = (yeniBas.z + 1) % derinlik;
		break;
		case Asagi:       //aşağı ok tuşuna basıldı ise  
			yeniBas.z = (yeniBas.z == 0) ? derinlik - 1 : yeniBas.z - 1;
		break;
	}

	// Yılanın başının yeni konumu
	govde_.insert(govde_.begin(),yeniBas);

    // kuyruğun sonunu sil
	govde_.pop_back();
	
	//yılan çarpışmasının kontrolü
	auto ilk = govde_.begin() + 1;
	auto kuyruk = govde_.end();

	while(ilk != kuyruk)
	{
		// Yılan kendi ile çarpıştı mı?
		if(*ilk == yeniBas)
		{
			yenidenBasla();       // çarpışma ile yeniden başlat   
			yemYeriniAyarla();    // yemin konumunu belirle
			kalanCan_--;          // yaşam puanını düşür.

			if(kalanCan_ == 0)
			{       
				skor_ = 0;       //yaşam puanı bitti ise oyunu başa sar  
				kalanCan_ = baslaCan_;
			}

			return;
		}
		ilk++;
	}

	// Yem ile çarpışıldı mı?
	if( yeniBas == yem_ )
	{
		yemYeriniAyarla();    		//yemin yeni konumunu ayarla ve skoru arttır.
		skor_ += 1;
                periyot -= 1;    		//hız için periyodu düşür.

		govde_.push_back(yeniKuyruk);    //kuyruğa yeni bir parça ekle 
	}

}

/* 
 * Yılanın klavye hareketlerinin kontrol fonksiyonu 
 */
void Yilan::hareketKontrol(int tus)
{
     switch(tus){
        case 'q':          
        case 'Q':          //q veya Q tuşu ile çıkış
	        exit(0); 
            break;
        case 'p':
        case 'P':           //p veya P ile oyunu durdurma
	        this->duraklama_ = !this->duraklama_;
            break;
        case 'r': 
        case 'R':           //r veya R ile oyunu yeniden başlatma
            this->yenidenBasla(); 
            break;
        case ']':           //(Ctrl-9) yılanı hızlandırma
        {
            int per = periyot * PERIYOTSTEP;   
 
        	if(periyot - per >= PERIYOTMIN)
		        periyot -= per;

            break;
        }
        case '[':            //(Ctrl-8) yılanı yavaşlatma
        {
            int per = periyot * PERIYOTSTEP;  

        	if(periyot + per <= PERIYOTMAX)
		        periyot += per;

            break;
        }
        case 'b':
        case 'B':           //Yüzey aç/kapat
	        this->yuzey_ = !this->yuzey_;
            break;
        case 'l':           //Işıklandırmayı aç/kapat
            if(glIsEnabled(GL_LIGHTING))
                glDisable(GL_LIGHTING); 
            else
                glEnable(GL_LIGHTING);
            break;
        case GLUT_KEY_RIGHT:
	        this->tasi(Sag); 
            break;
        case GLUT_KEY_LEFT:
            this->tasi(Sol); 
            break;
        case GLUT_KEY_UP:
            this->tasi(Yukari); 
            break;
        case GLUT_KEY_DOWN:
            this->tasi(Asagi); 
            break;
    }

}

/* 
 * Oyunu sahneleyen fonksiyonu 
 */
void Yilan::sahnele(){

    //Derinlik ve renk tamponunu boşalt
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Geçerli matrisi kaydet
    glPushMatrix();

    //Kamera konumu
    glRotatef(-90, 1.0, 0.0, 0.0);
    glRotatef(0, 0.0, 1.0, 0.0);

    glTranslatef(-(float)genislik/2, -(float)yukseklik/2, -(float)derinlik/2);
       
    this->ciz();           

    //Kaydedilen matrisi çağır
    glPopMatrix();

    this->puaniCiz();

    glutSwapBuffers();  //tamponları yer değiş
}

/* 
 *Yılanı yeniden başlatma fonksiyonu 
 */
void Yilan::yenidenBasla()
{
     duraklama_=true;    //oyunu durdur

     yon_ = Yukari;   //Başlangıçta yukarı hareket et
     
     int x = genislik/2;
     int y = yukseklik/2;
     int z = derinlik/2;

     govde_.clear();  //Gövdeyi sil

     /// yedi tane boğuma sahip bir yılan ile başlayalım
     for(int i = 0; i < 7 ; i++)
	     govde_.push_back(Kup(x, y, z - i) );

     this->yemYeriniAyarla();       //Yemin yerini ayarlayalım
}

/* 
 *Yemin yerini ayarlama fonksiyonu 
 */
void Yilan::yemYeriniAyarla()
{
	do{
		//yemin x,y,< koordinatı 
		yem_.ayarla(rand()%genislik,rand()%yukseklik,rand()%derinlik);     
  
	}while(*this == yem_);	// Yem yılanın üzerinde oluşmamalıdır!
}

/* 
 *Yılan cizen fonsiyon
 */
void Yilan::ciz()
{
	if(yuzey_)
		glCallList(yuzeyList);    //Yüzeyi çiz 
    
	this->yemiCiz();

	//Yılaı oluşturan kupleri çiz
  	for(auto &k: govde_)
	{
		glPushMatrix();
			glTranslatef(k.x, k.y, k.z);
	                glCallList(kupList);    //küp listesi çağrımı
	    glPopMatrix();
	}

}

/* 
 *Yılanın yemini cizen fonsiyon 
 */
void Yilan::yemiCiz()
{
	glColor4f(0.8, 0.0, 0.0, 0.4);  //saydam kırmızı renk

    glPushMatrix();
   		glTranslatef(yem_.x, /*yem_.y*/ 0, yem_.z);
	        glCallList(yemList);    //top listesi çağrımı
    glPopMatrix();
}

/* 
 * \brief Puan durumunu cizen fonsiyon
 */
void Yilan::puaniCiz()
{
	glDisable(GL_LIGHTING); 
	glColor3f(0,1,0);

	float orthoSize = max(max(genislik,yukseklik),derinlik)*0.7;
	void *font = GLUT_BITMAP_TIMES_ROMAN_24; 
	//void *font = GLUT_BITMAP_HELVETICA_18;

	stringstream stremstr;

	stremstr<<"SKOR: "<<skor_<<" \t HIZ: ";
	stremstr<< fixed << setprecision(2) << 1000.0/periyot;
	stremstr<<" \t YASAM PUANI: "<<kalanCan_;

	glRasterPos3f(-orthoSize*0.5, +orthoSize*0.9, orthoSize*0.9);     //Görüntüyü konumlandır

	string str = stremstr.str();

	for (auto &c : str){		
		glutBitmapCharacter(font, c );      //Bitmap font çizimi
	}

	glEnable(GL_LIGHTING);
}

/* 
 *Yılanın hareket fonksiyonu 
 */
void Yilan::tasi(Yon in_yonTusu){

	if(in_yonTusu != yon_)
	{
		yon_ = in_yonTusu;
	}

	duraklama_ = false;  //Duruyorsa devam et
}

bool Yilan::operator==(const Kup& in_orig)
{

	for(auto& eleman : govde_)
	{
		if(eleman != in_orig)
			return false;
	}

	return true;
}

