/* 
 * File:   Kup.hpp
 * Author: kyellow
 *
 * Created on December 11, 2012, 2:58 PM
 */

#ifndef KUP_HPP
#define	KUP_HPP

#define KUP_RGB 0.0f, 0.8f, 0.8f  ///< Kübe ait renk değerleri 
#define KUP_AL 0.3f       
#define KUP_AH 0.6f

/*
 * \brief Kup sınıfı, oyun alanındaki her bir birimi temsil eder
 */
class Kup {

public :       
	/*
	 * \brief Varsayılan yapılandırıcı
	 */
	Kup()
        : x(0), y(0), z(0) { }
	/*
	 * \brief Varsayılan yıkıcı
	 */
	virtual ~Kup() { }
	/*
	 * \brief Parametreli yapılandırıcı
	 * \param in_x X koordinat değerini alır
	 * \param in_y Y koordinat değerini alır
	 * \param in_z Z koordinat değerini alır
	 */
	Kup(int in_x, int in_y, int in_z)
        	: x(in_x), y(in_y), z(in_z) { }
	/*
	 * \brief Kopya yapılandırıcı
	 * \param in_orig Orjinal küp değerini alır
	 */
	Kup(const Kup& in_orig)
        	: x(in_orig.x), y(in_orig.y), z(in_orig.z) { }
	/*
	 * \brief Eşit değil karşılaştırma operatörü
	 * \param in_orig Orjinal küp nesnesini alır
	 *
	 * \return Karşılaştırma sonucunu döner
	 */
	bool operator!=(const Kup& in_orig)
	{
		 return (this->x != in_orig.x) || 
		        (this->y != in_orig.y) ||  
		        (this->z != in_orig.z);    
	}
	/*
	 * \brief Eşittir karşılaştırma operatörü
	 * \param in_orig Orjinal küp nesnesini alır
	 *
	 * \return Karşılaştırma sonucunu döner
	 */
	bool operator==(const Kup& in_orig)
	{
		return (this->x == in_orig.x) && 
		       (this->y == in_orig.y) &&  
		       (this->z == in_orig.z);      
	}
	/*
	 * \brief Kup kordinatlarını yeniden ayarlar
	 * \param in_x Yeni X koordinat değerini alır
	 * \param in_y Yeni Y koordinat değerini alır
	 * \param in_z Yeni Z koordinat değerini alır
	 */
	void ayarla(int in_x, int in_y, int in_z)
	{
		this->x = in_x;
		this->y = in_y;
		this->z = in_z;
	}


	int x;		///< X koordinatı 
	int y;		///< Y koordinatı
	int z; 		///< Z koordinatı
};

#endif	/* KUP_HPP */

