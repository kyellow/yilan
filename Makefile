# Environment
CXX=g++
# Link Libraries and Options
LDLIBSOPTIONS=-lGL -lGLU -lglut

all : grafik

grafik: main.o yilan.o
	$(CXX) -o grafik $^ $(LDLIBSOPTIONS)

%.o : %.cpp
	$(CXX) -std=c++11 -g -o $@ -c $<
