/* 
 * File:   main.cpp
 * Author: kyellow
 *
 * Created on December 10, 2012, 9:38 PM
 */

#include "yilan.hpp"

Yilan yilan;

/* 
 *Normal klavye tuş olaylarını işleyen fonksiyon 
 */
void klavye_girisi( unsigned char k, int x, int y){

    yilan.hareketKontrol(k);
    
    glutPostRedisplay();
}

/* 
 *Özel klavye tuş olaylarını işleyen fonksiyon 
 */
void ozelTus(int key, int x, int y){
	
    yilan.hareketKontrol(key);
}

/* 
 *Periyodik hareket fonksiyonu 
 */
void timer(int t){
    
    glutTimerFunc(yilan.periyot, timer, 0);    // zamanlayıcıyı yeniden ayarla

	yilan.periyodikKontrol();

    glutPostRedisplay();
}

/* 
 * OPENGL sahneleme geridönüş fonksiyonu 
 */
void sahnele(void){

    yilan.sahnele();
}

/* 
 * Pencere boyutları değişince çağırılacak fonksiyon 
 */
void pencere_resize (int w, int h)
{
   //Görüntü alanı olarak tüm pencereye uygula 
   glViewport (0, 0, (GLsizei) w, (GLsizei) h);

   //projeksiyon matrisini kullan     
   glMatrixMode (GL_PROJECTION);

   //Matrisi resetle
   glLoadIdentity();

   float orthoSize = std::max(std::max(yilan.genislik,yilan.yukseklik),yilan.derinlik)*0.7;

   if (w <= h)  //Doğru ortho uygula
      glOrtho (-orthoSize, orthoSize, -orthoSize*(GLfloat)h/(GLfloat)w,
             orthoSize*(GLfloat)h/(GLfloat)w, -orthoSize, orthoSize);
   else
      glOrtho (-orthoSize*(GLfloat)w/(GLfloat)h,
             orthoSize*(GLfloat)w/(GLfloat)h, -orthoSize, orthoSize, -orthoSize, orthoSize);

   //Modelview matrisine geri dön
   glMatrixMode(GL_MODELVIEW);

   //Matrisi resetle
   glLoadIdentity();
   
   glutPostRedisplay();
}

/* 
 * Temel main fonksiyon 
 */
int main(int argc, char **argv){

    //OpenGL yapılandırma fonksiyonları ( glutInit* )
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
 
    //  glutInitWindowSize (1024, 768);
    //  glutInitWindowPosition (0, 0);

    glutCreateWindow ("Yilan");
    glutFullScreen();
 
    yilan.initOpenGL ();    //OpenGL ön hazılıklarını yap

    //Geridönüş fonksiyonları
    glutKeyboardFunc(klavye_girisi);
    glutSpecialFunc(ozelTus);
    glutDisplayFunc(sahnele);
    glutReshapeFunc(pencere_resize);

    //Fareyi ekranda gizle
    glutSetCursor(GLUT_CURSOR_NONE);

    //zamanlayıcı fonksiyonu
    glutTimerFunc(yilan.periyot, timer, 0);

    //Olay işleme döngüsüne gir
    glutMainLoop();

    exit(EXIT_SUCCESS);
}
