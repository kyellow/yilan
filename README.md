# Yilan

![Screenshot](https://gitlab.com/kyellow/yilan/raw/master/demo.png)

## Description
This snake game was develped as final project of Computer Graphics course during the fall semester of 2012-2013. 

## How to run

### Requirements
This project based upon the FreeGLUT(OpenGL Utility Toolkit) library. You must have the freeglut libraries
and header files installed to build this project from source.

### Building
Steps for building and testing (assuming GNU make)

    $ make
    $ ./grafik

## How to use

After some loading time you will be able to see

- 7 node as snake
- red dot as forage
- blue surface as playground
- lifes, score and speed information as green text

You can

- 'q' or 'Q' key to exit
- 'p' or 'P' key to pause
- 'r' or 'R' key to restart
- 'b' or 'B' key to turn on/off surface
- 'l' or 'L' key to turn on/off lighting
- ']' key to speedup
- ']' key to speeddown
-  UP,DOWN,RIGHT,LEFT arrow keys to move


## References
- I used the tutorial at opengl-tutorial.org 
 
